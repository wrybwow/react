import React from 'react';


export class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
    this.filterList = this.filterList.bind(this);
  }

  filterList(e){
    let filteredList = this.props.data.items.filter(function(item){
      return item.toLowerCase().search(e.target.value.toLowerCase())!== -1;
    });
    this.setState({items:filteredList});
  }
  
  
  componentDidMount() {
    fetch("https://cat-fact.herokuapp.com/facts")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // Примітка: важливо обробляти помилки саме тут,
        // а не в блоці catch (), щоб не перехоплювати
        // виключення з помилок в самих компонентах.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  
  c
  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Помилка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Завантаження...</div>;
    } else {
      return (
        <div>
         <h2>{this.props.data.title}</h2>
         <input placeholder="Search" onChange={this.filterList}/> 
        <ul >
          {this.state.items.map(item => (
            <li key={item._id}>
              {item.text} 
            </li>
          ))}
        </ul>
        </div>
      );
    }
  }
}

