import React from 'react';


export default class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      filteredItems: []
    };
    this.filterList = this.filterList.bind(this);
  }

  filterList(e){
    const filteredList = this.state.items.filter(function(item){
        return item.text.toLowerCase().search(e.target.value.toLowerCase()) !== -1;
    });
    
    this.setState({filteredItems: filteredList});
  }
  
  componentDidMount() {
    fetch("https://cat-fact.herokuapp.com/facts")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
            filteredItems: result
          });
        },
        // Примітка: важливо обробляти помилки саме тут,
        // а не в блоці catch (), щоб не перехоплювати
        // виключення з помилок в самих компонентах.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, filteredItems } = this.state;
    if (error) {
      return <div>Помилка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Завантаження...</div>;
    } else {
      return (
        <div>
         
          <input placeholder="Search" onChange={this.filterList} />
        <ul >
          {filteredItems.map(item => (
            <li key={item._id}>
              {item.text} 
            </li>
          ))}
        </ul>
        </div>);
    }
  }
}

